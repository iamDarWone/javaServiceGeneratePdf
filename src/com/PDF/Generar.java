package com.PDF;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.konylabs.android.KonyMain;

public class Generar {

	
  static final String NOMBRE_DIRECTORIO = "Compensar";
 static final String NOMBRE_DOCUMENTO = "appSalud.pdf";
 static Document documento;
 static boolean generoPdf = false;
 static File f;
private static Activity actividad;
static Font font1,font2,font3,font4;



static public 	boolean generarDocumento(String idnombreDocumento ,
										String titulo ,
										String nombreUsuario,
										String especialidad,
										String fecha,
										String direccion,
										String hora,
										String pago,
										String autorizacion) 
{
	
	Context context = KonyMain.getActivityContext();
	actividad = (Activity)context;	
	
   documento=new Document(PageSize.LETTER,20,20,20,20);
	// Creamos el flujo de datos de salida para el fichero donde
				// guardaremos el pdf.
   Log.d("StandardLib","activida nombreDocumento" + idnombreDocumento ); 
	try {

		// Creamos el fichero con el nombre que deseemos.
			String nombreArchivo = idnombreDocumento.concat(".pdf");
			File f = crearFichero(nombreArchivo);
		
		
		
		Log.d("StandardLib","activida creada" + f );  
		if (f != null ){
			// Creamos el flujo de datos de salida para el fichero donde
			// guardaremos el pdf.
			FileOutputStream ficheroPdf = new FileOutputStream(
					f.getAbsolutePath());

			// Asociamos el flujo que acabamos de crear al documento.
			PdfWriter writer = PdfWriter.getInstance(documento, ficheroPdf);		
	
			
			 Rectangle rct = new Rectangle(10,30,540,760);
		
	            //Definimos un nombre y un tamaño para el PageBox los nombres posibles son: “crop”, “trim”, “art” and “bleed”.
	            writer.setBoxSize("art", rct);
	            HeaderFooter event = new HeaderFooter();
	            writer.setPageEvent(event);

			
			// Abrimos el documento.
			documento.open();
			
			// Añadimos datos autor.
			documento.addCreator("Dsalcedo");
			documento.addAuthor("Dsalcedo");
			
			// Insertamos una imagen que se encuentra en los recursos de la
			// aplicacion.	
			cargarImagen();
			saltoLinea();	        
			
			// fuentes
	        cargarFuentes();
	        agregarTablaTitulo(new BaseColor(252, 123, 10),"AUTORIZACIÓN CITA MÉDICA ",font1);
	            
			// Añadimos un titulo con una fuente personalizada.
			
	         
			documento.add(new Paragraph("Nombre Profesional : "+titulo, font2));
	            
			documento.add(new Paragraph("Nombre Usuario : "+nombreUsuario, font2));
			saltoLinea(2);	
			
			agregarTablaTitulo(new BaseColor(73, 73, 73),"ESPECIALIDAD ",font3);
			documento.add(new Paragraph(especialidad, font2));
			saltoLinea();
			agregarTablaTitulo(new BaseColor(73, 73, 73),"FECHA ",font3);
			documento.add(new Paragraph(fecha, font2));
			saltoLinea();
			agregarTablaTitulo(new BaseColor(73, 73, 73),"DIRECCIÓN ",font3);
			documento.add(new Paragraph(direccion, font2));
			saltoLinea();
			agregarTablaTitulo(new BaseColor(73, 73, 73),"HORA ",font3);
			documento.add(new Paragraph(hora, font2));
			saltoLinea();
			agregarTablaTitulo(new BaseColor(73, 73, 73),"PAGO ",font3);
			documento.add(new Paragraph(pago, font2));
			saltoLinea();
			agregarTablaTitulo(new BaseColor(73, 73, 73),"AUTORIZACIÓN ",font3);
			documento.add(new Paragraph(autorizacion, font2));
			saltoLinea(1);
			
			 String nota = new String("No Olvide: \n" +
						"Documento de identidad \n" +
						"Exámenes médicos (si aplica) \n" +
						"Llegar 20 minutos antes \n" +
						"Si desea modificar su cita hágalo con 1 hora de antelación");
		
			 Paragraph parrafo = new Paragraph(nota, font4);
			parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
			
			 documento.add(parrafo);
				
				


		
			 viewPdf(f);   
			Toast.makeText(actividad.getApplicationContext(), "Generado! "+f.getAbsolutePath(), Toast.LENGTH_LONG).show();
			
			generoPdf = true;	
		}   
      
	} catch (DocumentException e) {
		Toast.makeText(actividad.getApplicationContext(), "es posible que no leas el archivo! necesitas mas espacio de almacenamiento "+e.getMessage(), Toast.LENGTH_LONG).show();
	    
		Log.e("error ", e.getMessage());

	} catch (IOException e) {
		
		Toast.makeText(actividad.getApplicationContext(), "es posible que no leas el archivo! necesitas mas espacio de almacenamiento "+e.getMessage(), Toast.LENGTH_LONG).show();
	    
		Log.e("error ", e.getMessage());

	} finally {

		// Cerramos el documento.
		documento.close();
	}
	
	
	//generar ntificacaion para archivo pdf
	
		
	

	
	
	return  generoPdf;
	}

public static void viewPdf(File file) {
    Context context = KonyMain.getActivityContext();
    //File file = new File("/storage/emulated/0/Download/Compensar/appSalud.pdf");

    Intent intent = new Intent(Intent.ACTION_VIEW);

    intent.setDataAndType(Uri.fromFile(file),"application/pdf");

    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

       try {
        context.startActivity(intent);
       } catch (ActivityNotFoundException e) {
           // No application to view, ask to download one
                           Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                           marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
                           context.startActivity(marketIntent);
                       }
   }
       
   

/**
 * dependiendo de la conexion del dispositivo a internet 
 * mostrara una imagen en el documento
 */
public static void cargarImagen()  {
	// TODO Auto-generated method stub
	//sybven "http://1.bp.blogspot.com/-fC0E4WInU5o/USd9XsuVniI/AAAAAAAAA_w/mYXJQtTwFfU/s1600/Banner+Sybven.jpg"
	//compensar new URL("http://www.healthcorp.biz/wp-content/uploads/2014/12/Compensar.png")
	Image image;
	try {
		image = Image.getInstance(new URL("http://www.healthcorp.biz/wp-content/uploads/2014/12/Compensar.png"));
		image.scaleAbsolute(250, 100);		
		documento.add(image);
	} catch (BadElementException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	 catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
}

/**
 * todas las fuentes de la pagina
 */
public static void cargarFuentes() {
	// TODO Auto-generated method stub
	  font1 = new Font(Font.FontFamily.HELVETICA  , 20, Font.BOLD);
	 font1.setColor(BaseColor.WHITE);
	 font2 = new Font(Font.FontFamily.HELVETICA   , 12);
	 font2.setColor(BaseColor.BLACK);
      font3 = new Font(Font.FontFamily.COURIER, 14,Font.BOLD);
     font3.setColor(BaseColor.ORANGE);
     font4 = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD);
     font4.setColor(BaseColor.RED);
}



/**
 * Salto de linea  obtiene un numero 
 * el cual es el numero de veces que 
 * saltara de linea 
 */
public static void saltoLinea(int numeroVeces ) {
	// TODO Auto-generated method stub
	try {
		for (int i = 0; i < numeroVeces; i++) {
			documento.add(new Paragraph("\n"));
		}
		
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
}


/**
 * Salto de linea sin paramaetros 
 * hace un solo salto
 */
public static void saltoLinea() {
	// TODO Auto-generated method stub
	try {
			documento.add(new Paragraph("\n"));
	
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
}


/**
 * añade una tabla al cuerpo del pdf
 * con las propiedades color,cadena que se muestra
 * y la fuente que tendra dicha cadena
 * 
 * @param baseColor
 * @paramn string
 * @param font1
 */
public static void agregarTablaTitulo(BaseColor baseColor, String cadena, Font fuente) {
	// TODO Auto-generated method stub
		PdfPTable table = new PdfPTable(1); 	 
		table.setWidthPercentage(100);
	    PdfPCell cell1 = new PdfPCell(new Paragraph(cadena,fuente));
	    cell1.setBorder(Rectangle.NO_BORDER);
	    cell1.setPadding(10f);
	    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    cell1.setBackgroundColor(baseColor);
	    table.addCell(cell1);
	    table.setHorizontalAlignment(Element.ALIGN_CENTER);
	    try {
			documento.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
}


/**
 * Crea un fichero con el nombre que se le pasa a la funcion y en la ruta
 * especificada.
 * 
 * @param nombreFichero
 * @return
 * @throws IOException
 */
public static File crearFichero(String nombreFichero) throws IOException {
	File ruta = getRuta();
	File fichero = null;
	if (ruta != null){
		fichero = new File(ruta, nombreFichero);
	}
	/**else{
		fichero = new File(actividad.getFilesDir(), NOMBRE_DOCUMENTO);
	}*/
	return fichero;
}

/**
 * Obtenemos la ruta donde vamos a almacenar el fichero.
 * 
 * @return
 */
public static File getRuta() {

	// El fichero sera almacenado en un directorio dentro del directorio
	// Descargas
	File ruta = null;
	if (Environment.MEDIA_MOUNTED.equals(Environment
			.getExternalStorageState())) {
		ruta = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
				NOMBRE_DIRECTORIO);
				
		if (ruta != null) {
			if (!ruta.mkdirs()) {
				if (!ruta.exists()) {
					
					return null;
				}
			}
		}
	}
	return ruta;
}
/**
 * crea el header y footer de la pagina
 * 
 *
 */
static class HeaderFooter extends PdfPageEventHelper {

    public void onEndPage (PdfWriter writer, Document document) {
        Rectangle rect = writer.getBoxSize("art");
        //Cabecera
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_RIGHT, new Phrase("http://www.compensar.com/ | " +
                								"tlf : 444-1234"),
                rect.getRight(),rect.getTop() , 0);
        
        //PieString.format("page %d otra cosa", writer.getPageNumber())
        String nota = new String("El contenido de este mensaje puede ser información privilegiada y confidencial de Compensar.  ");
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase(nota),
                (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
       
    	}
	}

public static void redireccionarWEB(String url){
	  //"https://docs.google.com/gview?embedded=true&url=pruebaunobasededatos.esy.es/PDF/test.pdf"
	   Context context = KonyMain.getActivityContext();
	   Intent intent = new Intent(Intent.ACTION_VIEW);
	   intent.setData(Uri.parse(url));
	   context.startActivity(intent);
 }

public static void redireccionarWEB2(){
	  
	   Context context = KonyMain.getActivityContext();
	   Intent intent = new Intent(Intent.ACTION_VIEW);
	   intent.setData(Uri.parse("http://www.pruebaunobasededatos.esy.es/PDF/test.pdf"));
	   context.startActivity(intent);
}

}

